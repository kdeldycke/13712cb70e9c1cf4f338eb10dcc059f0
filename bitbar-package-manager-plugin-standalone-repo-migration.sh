set -x

rm -rf ./package-manager
git clone https://github.com/kdeldycke/bitbar-plugins.git ./package-manager
cd ./package-manager

git checkout master

git remote rm origin
git branch -D multi-linter
git branch -D package_manager_mas
git branch -D pull-request-template

git filter-branch --force --prune-empty --tree-filter "find . -type f \
    -and -not -path '*package_*' \
    -and -not -path './.git\/*' \
    -and -not -path './' \
    | xargs -I '{}' rm -f '{}'" -- --all

git filter-branch --force --prune-empty --tree-filter 'find . -type f -and -path "*/package_*" | xargs -I "{}" mv "{}" ./' -- --all

git filter-branch --force --msg-filter 'sed -r "s/#([0-9]+)/https:\/\/github.com\/matryer\/bitbar-plugins\/pull\/\1 /"' -- --all

git rebase --root

git reflog expire --all
git gc --aggressive --prune

git remote add origin https://github.com/kdeldycke/package-manager.git

git push -u origin master